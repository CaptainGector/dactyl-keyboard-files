#pragma once

#include "quantum.h"
//#include "print.h"

#define LAYOUT( \
		K05, K04, K03, K02, K01, K00, 	  K07, K08, K09, K0A, K0B, K0C, \
		K15, K14, K13, K12, K11, K10, 	  K17, K18, K19, K1A, K1B, K1C, \
		K25, K24, K23, K22, K21, K20, 	  K27, K28, K29, K2A, K2B, K2C, \
		K35, K34, K33, K32, K31, K30, 	  K37, K38, K39, K3A, K3B, K3C, \
				  K43, K42,            	          K49, K4A,           \
							K41, K40,  	  K47, K48,                     \
							K16, K46,  	  K4D, K1D,                     \
				  K26, K36,            	          K3D, K2D           \
									   								  \
) { \
		{ K00,   K01,   K02, K03, K04,   K05,  KC_NO }, \
		{ K10,   K11,   K12, K13, K14,   K15,  K16 }, \
		{ K20,   K21,   K22, K23, K24,   K25,  K26 }, \
		{ K30,   K31,   K32, K33, K34,   K35,  K36 }, \
		{ K40,   K41, 	K42, K43, KC_NO, KC_NO,K46 }, \
		\
		{ K07,   K08,   K09, K0A, K0B,   K0C,  KC_NO }, \
		{ K17,   K18,   K19, K1A, K1B,   K1C,  K1D }, \
		{ K27,   K28,   K29, K2A, K2B,   K2C,  K2D }, \
		{ K37,   K38,   K39, K3A, K3B,   K3C,  K3D }, \
		{ K47,   K48, 	K49, K4A, KC_NO, KC_NO,K4D} \
}
