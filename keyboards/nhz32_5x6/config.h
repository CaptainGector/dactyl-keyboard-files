/*
Copyright 2020 NeohertzParts

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//#include "config_common.h"

// Required to work with STM32 clones. (maybe?)
// https://github.com/qmk/qmk_firmware/issues/7204
#define FEE_MCU_FLASH_SIZE 64

/* USB Device descriptor parameter */
//#define VENDOR_ID    0x1eed
//#define PRODUCT_ID   0x1FF1
//#define DEVICE_VER   0x0002
//#define MANUFACTURER NeohertzParts
//#define PRODUCT      Nhz32 5x6

/* key matrix size */
// doubled bcuz split keyboard
#define MATRIX_ROWS 10
#define MATRIX_COLS 7


// {B0, B1, B4, B5, B8, B9, B10} - Columns
// {A3, A2, A1, A0, A8}
#define MATRIX_ROW_PINS {A0, A1, A2, A3, A8}
//#define MATRIX_COL_PINS {B0, B1, B4, B5, B8, B9, B10}
#define MATRIX_COL_PINS  {B9, B8, B5, B4, B1, B0, B10}

// GPIO pins A0-A6 are for the columns (7 columns) (6 + thumb)
// GPIO pins B3-B7 are for the rows (5 rows)


#define DIODE_DIRECTION COL2ROW // diode points away from switch pin

// Serial configuration
//#define SOFT_SERIAL_PIN D0
//#define SELECT_SOFT_SERIAL_SPEED {#}

/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE

//#define RGBLIGHT_ANIMATIONS
//#define RGB_DI_PIN //B0
//#define RGBLED_NUM 1
//#define RGBLIGHT_DEFAULT_MODE RGBLIGHT_MODE_RGB_TEST
//#define WS2812_SPI SPID2

// Split keyboard stuff
#define SOFT_SERIAL_PIN A9
//#define SELECT_SOFT_SERIAL_SPEED {2}
#define SERIAL_USART_DRIVER SD1    // USART driver of TX pin. default: SD1
//#define SERIAL_USART_TX_PAL_MODE 7 // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
//#define SERIAL_USART_TIMEOUT 20    // USART driver timeout. default 20
#define MASTER_LEFT // change this to flash the other keyboard side?
#define SPLIT_USB_DETECT

/* Debounce reduces chatter (unintended double-presses) - set 0 if debouncing is not needed */
#define DEBOUNCE 5

/* define if matrix has ghost (lacks anti-ghosting diodes) */
//#define MATRIX_HAS_GHOST

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
//#define LOCKING_SUPPORT_ENABLE
///* Locking resynchronize hack */
//#define LOCKING_RESYNC_ENABLE
//
//#define NO_ACTION_MACRO
//#define NO_ACTION_FUNCTION

//#define NO_AUTO_SHIFT_ALPHA

/* Bootmagic Lite key configuration */
//#define BOOTMAGIC_LITE_ROW 0
//#define BOOTMAGIC_LITE_COLUMN 0
