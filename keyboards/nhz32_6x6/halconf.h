
#pragma once

#define HAL_USE_PWM TRUE

#define HAL_USE_SPI TRUE

// Split keyboard functionality
#define HAL_USE_SERIAL TRUE

#include_next <halconf.h>
