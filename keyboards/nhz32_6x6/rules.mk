# MCU name
MCU = STM32F103

# Bootloader selection
#BOOTLOADER = stm32duino
BOOTLOADER = stm32duino

#BOARD = STM32_F103_STM32DUINO

BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
MOUSEKEY_ENABLE = no	# Mouse keys
EXTRAKEY_ENABLE = no	# Audio control and System control
CONSOLE_ENABLE = yes	# Console for debug
COMMAND_ENABLE = yes    # Commands for debug and configuration

# This is a split keyboard.
SPLIT_KEYBOARD = yes
SERIAL_DRIVER = usart

# RGB Backlight stuff
#BACKLIGHT_ENABLE = yes
#RGBLIGHT_ENABLE = no
#RGBLIGHT_DRIVER = WS2812
#WS2812_DRIVER = bitbang

#SRC += matrix.c
#QUANTUM_LIB_SRC += i2c_master.c
