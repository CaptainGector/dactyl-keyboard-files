#pragma once

#include "quantum.h"
//#include "print.h"


/*#define LAYOUT( \
		K05, K04, K03, K02, K01, K00, 	  K07, K08, K09, K0A, K0B, K0C, \
		K15, K14, K13, K12, K11, K10, 	  K17, K18, K19, K1A, K1B, K1C, \
		K25, K24, K23, K22, K21, K20, 	  K27, K28, K29, K2A, K2B, K2C, \
		K35, K34, K33, K32, K31, K30, 	  K37, K38, K39, K3A, K3B, K3C, \
		K45, K44, K43, K42, K41, K40, 	  K47, K48, K49, K4A, K4B, K4C, \
				  K53, K52,        					K59, K5A,       	\
							K51, K50,  	 K57, K58,               		\
							K26, K56,  	 K5D, K2D,               		\
				  K36, K46,            	            K4D, K3D            \
) { \
		{ K00,   K01,   K02, K03, K04,   K05,  KC_NO }, \
		{ K10,   K11,   K12, K13, K14,   K15,  KC_NO }, \
		{ K20,   K21,   K22, K23, K24,   K25,  K26 }, \
		{ K30,   K31,   K32, K33, K34,   K35,  K36 }, \
		{ K40,   K41, 	K42, K43, K44,   K45,  K46 }, \
		{ K50,   K51, 	K52, K53, KC_NO, KC_NO,K56 }, \
		\
		{ K07,   K08,   K09, K0A, K0B,   K0C,  KC_NO }, \
		{ K17,   K18,   K19, K1A, K1B,   K1C,  KC_NO }, \
		{ K27,   K28,   K29, K2A, K2B,   K2C,  K2D }, \
		{ K37,   K38,   K39, K3A, K3B,   K3C,  K3D }, \
		{ K47,   K48,   K49, K4A, K4B,   K4C,  K4D }, \
		{ K57,   K58, 	K59, K5A, KC_NO, KC_NO,K5D} \
}*/