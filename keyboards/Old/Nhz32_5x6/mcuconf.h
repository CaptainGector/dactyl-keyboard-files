#pragma once

// Split keyboard functionality

#include_next <mcuconf.h>

#undef STM32_PWM_USE_TIM1
#define STM32_PWM_USE_TIM1 TRUE

#ifdef STM32_SERIAL_USE_USART1
#undef STM32_SERIAL_USE_USART1
#endif
#define STM32_SERIAL_USE_USART1 TRUE
