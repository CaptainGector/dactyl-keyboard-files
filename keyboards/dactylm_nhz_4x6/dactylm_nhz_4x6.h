/* Copyright 2020 NeohertzParts
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "quantum.h"
#include "i2c_master.h"

#define I2C_ADDR        0b0100000
#define I2C_ADDR_WRITE  ( (I2C_ADDR<<1) | I2C_WRITE )
#define I2C_ADDR_READ   ( (I2C_ADDR<<1) | I2C_READ  )
#define IODIRA          0x00            // i/o direction register
#define IODIRB          0x01
#define GPPUA           0x0C            // GPIO pull-up resistor register
#define GPPUB           0x0D
#define GPIOA           0x12            // general purpose i/o port register (write modifies OLAT)
#define GPIOB           0x13
#define OLATA           0x14            // output latch register
#define OLATB           0x15

#define DACTYL_I2C_TIMEOUT 100

//extern i2c_status_t io_expander_status;
extern i2c_status_t io_expander_status;
uint8_t init_mcp23018(void);
uint8_t init_PCF8575(void);


enum expander_type{PCF8575, MCP23017};
enum mcu_breakout{TEENSY, PROMICRO};

#define LAYOUT_4x6( \
		K05, K04, K03, K02, K01, K00, 	  K07, K08, K09, K0A, K0B, K0C, \
		K15, K14, K13, K12, K11, K10, 	  K17, K18, K19, K1A, K1B, K1C, \
		K25, K24, K23, K22, K21, K20, 	  K27, K28, K29, K2A, K2B, K2C, \
				  K33, K32,            	          K39, K3A,           \
							K31, K30,  	  K37, K38,                     \
							K06, K36,  	  K3D, K0D,                     \
				  K16, K26,            	          K2D, K1D           \
									   								  \
) { \
		{ K00,   K01,   K02, K03, K04,   K05,  K06 }, \
		{ K10,   K11,   K12, K13, K14,   K15,  K16 }, \
		{ K20,   K21,   K22, K23, K24,   K25,  K26 }, \
		{ K30,   K31, 	K32, K33, KC_NO, KC_NO,K36 }, \
		\
		{ K07,   K08,   K09, K0A, K0B,   K0C,  K0D }, \
		{ K17,   K18,   K19, K1A, K1B,   K1C,  K1D }, \
		{ K27,   K28,   K29, K2A, K2B,   K2C,  K2D }, \
		{ K37,   K38, 	K39, K3A, KC_NO, KC_NO,K3D} \
}
