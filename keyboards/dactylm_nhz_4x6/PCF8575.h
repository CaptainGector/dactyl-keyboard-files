// Yes, by default the PCF8575 has the same address as the MCP23017.
// I change mine to 0b0100100 (0x48) instead so the program can figure out
// which.

#define PCF_I2C_ADDR        0b0100100
#define PCF_I2C_ADDR_WRITE  ( (PCF_I2C_ADDR<<1) | I2C_WRITE )
#define PCF_I2C_ADDR_READ   ( (PCF_I2C_ADDR<<1) | I2C_READ  )


// PCF8575 is an open-drain device. That means we have to tie the column pins
// to VCC through a resistor
